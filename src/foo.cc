#include <map>
#include <algorithm>
#include <functional>
#include <memory>
#include <stdexcept>
#include <string>
#include <istream>
#include <array>
//#include <ifstream>

#include <pybind11/pybind11.h>

typedef std::function< pybind11::module & (std::string const &) > ModuleGetter;

void bind_pyistream_0(std::function< pybind11::module &(std::string const &namespace_) > &M);

PYBIND11_MODULE(pyistream, root_module) {
	root_module.doc() = "pyistream module";

	std::map <std::string, pybind11::module> modules;
	ModuleGetter M = [&](std::string const &namespace_) -> pybind11::module & {
		auto it = modules.find(namespace_);
		if( it == modules.end() ) throw std::runtime_error("Attempt to access pybind11::module for namespace " + namespace_ + " before it was created!!!");
		return it->second;
	};

	modules[""] = root_module;

	static std::vector<std::string> const reserved_python_words {"nonlocal", "global", };

	auto mangle_namespace_name(
		[](std::string const &ns) -> std::string {
			if ( std::find(reserved_python_words.begin(), reserved_python_words.end(), ns) == reserved_python_words.end() ) return ns;
			else return ns+'_';
		}
	);

	std::vector< std::pair<std::string, std::string> > sub_modules {
		{"", "pyistream"},
	};
	for(auto &p : sub_modules ) modules[p.first.size() ? p.first+"::"+p.second : p.second] = modules[p.first].def_submodule( mangle_namespace_name(p.second).c_str(), ("Bindings for " + p.first + "::" + p.second + " namespace").c_str() );


	bind_pyistream_0(M);

}

void bind_pyistream_0(std::function< pybind11::module &(std::string const &namespace_) > &M) {
		M("pyistream").def("get_from_stream1", [](std::istream& ss) -> void{ 
			std::array<char, 262> buf;
			ss.getline(buf.data(), buf.size());
			  std::string a(&(buf[0]),10);
			  printf("->%s<-\n",a.c_str()); } , "OK");

		M("pyistream").def("get_from_stream2", [](pybind11::object& ss) -> void{ 

			  pybind11::object res = ss.attr("read")();
			  pybind11::print(pybind11::str(res));


			  } , "OK");

		M("pyistream").def("get_from_stream3", []() -> void{ 
		
		
			  //printf("->%s<-\n",a.c_str()); 
			  printf("->OKKK<-\n"); 
			  
			  
			  } , "OK");

}




