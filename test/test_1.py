import re
import sys, os, math


def python_label():
    return str("")
    v = sys.version_info
    impl = ""
    try:
        pypyv = sys.pypy_version_info
        impl = "pypy"
        print(pypyv)
    except:
        pass
    a = str(impl) + str(v[0]) + "." + str(v[1]) + "." + str(v[2])
    print(a)
    return a


def update_path():
    try:
     os.add_dll_directory(os.path.abspath(os.path.join(os.pardir, python_label())))
     os.add_dll_directory(os.getcwd())
     for val in str(os.getenv("PATH")).split(',:;'):
       os.add_dll_directory(os.path.abspath(val))
     for val in str(os.getenv("LD_LIBRARY_PATH")).split(',:;'):
       os.add_dll_directory(os.path.abspath(val))
     for val in str(os.getenv("DYLD_LIBRARY_PATH")).split(',:;'):
       os.add_dll_directory(os.path.abspath(val))
    except:
     pass
    return [os.path.abspath(os.path.join(os.pardir, python_label()))] + [os.getcwd()] + sys.path

sys.path = update_path()


from pyistream import pyistream as ps


def test_1():
    print(dir(ps))
    import gzip
    f=gzip.open('inputtest_1.gz', 'rb')
    print("OK1")
    ps.get_from_stream3()
    print("OK1222")
    
    ps.get_from_stream2(f)
    print("OK3")
    ps.get_from_stream1(f)
    print("OK2")
    return 0


if __name__ == "__main__":
    result = 1
    try:
        result = test_1()
    except:
        result = 1
    sys.exit(result)
